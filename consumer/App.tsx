import React from 'react';
import {Text, View} from 'react-native';
import {MyStack} from 'navigation/NavStack';

const App = () => {
  return (
    <View style={{flex:1, justifyContent: 'center', alignContent: 'center', backgroundColor: 'red'}}>
      <Text>stuff</Text>
      <MyStack />
    </View>
  );
};

export default App;