## to test this

In the navigation folder:
1. Run ```npm i```
2. Run ```npm pack```

In the consumer folder.

1. ```npm install react-native-safe-area-context react-native-screens```
2. ```npm install ../navigation/navigation-0.0.1.tgz```
3. cd into ios and run ```pod install```
4. cd back up and run ```npm run ios```
5. The app should start successfully and show you a navigation stack.